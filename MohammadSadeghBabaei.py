# Initialize a variable to store the sum
sum = 0

# Loop through all the numbers from 1 to 999 (inclusive)
for i in range(1, 1000):
    # Check if the number is a multiple of 3 or 5
    if i % 3 == 0 or i % 5 == 0:
        # If it is, add it to the sum
        sum += i

# Print the total sum
print(sum)
